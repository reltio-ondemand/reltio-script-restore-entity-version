# Restore Entity Version #

# Description #

This script is used to restore entities to an old version based on timestamp in the past

# How to run script #

* Build project or download jar (restore-entity-version.jar) from {{Project}}/jar/ location


* Usage
```
     nohup java -jar -Xmx2G -Xms2G restore-entity-version.jar {{properties-file-path}} > {output-log-filename} 2>&1 &
```
* Properties file
```
     reltioUrl=https://tst-01.reltio.com
     authUrl=https://auth-stg.reltio.com/oauth/token
     tenant=jas88zr17
     timestamp=1599153360000
     authUser=ramesh.thetakali@reltio.com
     authUserPwd=***
     entityType=HCP
     readOnly=true
     threadCount=1                  #no of threads to run (MAX_THREADS=100, default=1)
     filePath=<path-to-input-file>  #Pipe delimited
```
* Format of Pipe demilited input file for entities*
```
    1dtJliW6|HMS|PIJ09GB1E2
```
* Example:
```
     nohup java -jar -Xmx2G -Xms2G restore-entity-version.jar restore.properties > log_09_04_1.txt 2>&1 &
```
