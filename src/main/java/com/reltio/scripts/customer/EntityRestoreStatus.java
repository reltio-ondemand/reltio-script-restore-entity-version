package com.reltio.scripts.customer;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class EntityRestoreStatus {
    
    public enum RestoreStatus {
        SUCCESSFUL, FAILED
    }
    
    @JsonProperty("entityUri")
    private String entityUri;
    @JsonProperty("restoreStatus")
    private RestoreStatus restoreStatus;
    @JsonProperty("restoreRequestBody")
    private String restoreRequestBody;
    @JsonProperty("exceptionMessage")
    private String exceptionMessage;
    @JsonProperty("restoreResponse")
    private String restoreResponse;
    @JsonProperty("timestamp")
    private String timestamp;
    @JsonProperty("restoreTimestamp")
    private String restoreTimestamp;
    
    public EntityRestoreStatus(String entityUri, String timestamp) {
        this.entityUri = entityUri;
        this.timestamp = timestamp;
    }
    
    public String getEntityUri() {
        return entityUri;
    }
    
    public void setEntityUri(String entityUri) {
        this.entityUri = entityUri;
    }
    
    public RestoreStatus getRestoreStatus() {
        return restoreStatus;
    }
    
    public void setRestoreStatus(RestoreStatus restoreStatus) {
        this.restoreStatus = restoreStatus;
    }
    
    public String getRestoreRequestBody() {
        return restoreRequestBody;
    }
    
    public void setRestoreRequestBody(String restoreRequestBody) {
        this.restoreRequestBody = restoreRequestBody;
    }
    
    public String getExceptionMessage() {
        return exceptionMessage;
    }
    
    public void setExceptionMessage(String exceptionMessage) {
        this.exceptionMessage = exceptionMessage;
    }
    
    public String getRestoreResponse() {
        return restoreResponse;
    }
    
    public void setRestoreResponse(String restoreResponse) {
        this.restoreResponse = restoreResponse;
    }
    
    public String getTimestamp() {
        return timestamp;
    }
    
    public String getRestoreTimestamp() {
        return restoreTimestamp;
    }
    
    public void setRestoreTimestamp(String restoreTimestamp) {
        this.restoreTimestamp = restoreTimestamp;
    }
    
    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        
        EntityRestoreStatus that = (EntityRestoreStatus) o;
        
        if (entityUri != null ? !entityUri.equals(that.entityUri) : that.entityUri != null) return false;
        if (restoreStatus != that.restoreStatus) return false;
        return restoreRequestBody != null ? restoreRequestBody.equals(that.restoreRequestBody) : that.restoreRequestBody == null;
    }
    
    @Override
    public int hashCode() {
        int result = entityUri != null ? entityUri.hashCode() : 0;
        result = 31 * result + (restoreStatus != null ? restoreStatus.hashCode() : 0);
        result = 31 * result + (restoreRequestBody != null ? restoreRequestBody.hashCode() : 0);
        return result;
    }
    
    @Override
    public String toString() {
        return "EntityRestoreStatus{" + "entityUri='" + entityUri + '\'' + ", restoreStatus=" + restoreStatus + ", restoreRequestBody='" + restoreRequestBody + '\'' + ", exceptionMessage='" + exceptionMessage + '\'' + ", restoreResponse='" + restoreResponse + '\'' + ", timestamp='" + timestamp + '\'' + ", restoreTimeStamp='" + restoreTimestamp + '\'' + '}';
    }
}
