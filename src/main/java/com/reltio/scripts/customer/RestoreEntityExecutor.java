package com.reltio.scripts.customer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.reltio.cst.exception.handler.GenericException;
import com.reltio.cst.exception.handler.ReltioAPICallFailureException;
import com.reltio.cst.service.ReltioAPIService;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class RestoreEntityExecutor implements Callable<List<EntityRestoreStatus>> {
    
    private static final String SHOW_ALL = "showAll";
    private static final String MAX_PARAM = "max";
    private static final String ORDER = "order";
    private static final String SORT = "sort";
    private static final String FILTER = "filter";
    
    private List<String> batch;
    private ReltioAPIService apiService;
    private String apiUrl;
    private String tenantId;
    private boolean readOnly;
    private ObjectMapper objectMapper;
    private String versionTimestamp;
    private String entityType;
    private String tenantUrl;
    private boolean debugMode;
    
    public RestoreEntityExecutor(List<String> batch, String entityType, ReltioAPIService apiService, String apiUrl, String tenantId, String versionTimestamp, boolean readOnly, boolean debugMode) {
        this.batch = new ArrayList<>(batch);
        this.apiService = apiService;
        this.apiUrl = apiUrl;
        this.tenantId = tenantId;
        this.readOnly = readOnly;
        this.objectMapper = new ObjectMapper();
        this.versionTimestamp = versionTimestamp;
        this.entityType = entityType;
        this.tenantUrl = apiUrl +"/reltio/api/" + tenantId;
        this.debugMode = debugMode;
    }
    
    @Override
    public List<EntityRestoreStatus> call() throws Exception {
        List<EntityRestoreStatus> statusList = Lists.newArrayList();
        ObjectNode jsonResult = objectMapper.createObjectNode();
        for (String line : batch) {
            String[] record = line.split("\\|");
            String entity = record[0];
            String sourceSystem = record[1];
            String crosswalk = record.length ==3 ? record[2] : null;
            EntityRestoreStatus status = new EntityRestoreStatus(entity, versionTimestamp);
            if (sourceSystem.equals("Reltio")){
                status.setExceptionMessage("Restore not supported for 'Reltio' crosswalk");
                status.setRestoreStatus(EntityRestoreStatus.RestoreStatus.FAILED);
                statusList.add(status);
                continue;
            }
            
            try {
                String restoreTimeStamp = getHistoryTimestamp(entity, versionTimestamp);
                status.setRestoreTimestamp(restoreTimeStamp);
                System.out.println(String.format("\nRestoring entity: %s to timestamp: %s", entity, restoreTimeStamp));
                JsonNode historyEntity = getEntityAtTimestamp(entity, restoreTimeStamp);
                JsonNode crosswalks = historyEntity.get("crosswalks");
                String crosswalkValue = crosswalk == null ? getCrosswalkValueFromSource(sourceSystem, crosswalks) : crosswalk;
                JsonNode attributes = historyEntity.get("attributes");
                //process attributes
                ObjectNode attributesNode = objectMapper.createObjectNode();
                processAttributes(attributesNode, attributes, sourceSystem);
                jsonResult.put("type", "configuration/entityTypes/" + entityType);
                jsonResult.set("attributes", attributesNode);
                if (historyEntity.has("analyticsAttributes")) {
                    jsonResult.set("analyticsAttributes", historyEntity.get("analyticsAttributes"));
                }
                if (crosswalkValue != null) {
                    jsonResult.set("crosswalks", constructCrosswalks(sourceSystem, crosswalkValue));
                } else {
                    continue;
                }
                ArrayNode arrayNode = objectMapper.createArrayNode();
                arrayNode.add(jsonResult);
                String jsonPayload = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(arrayNode);
                String response;
                if (debugMode) System.out.println(String.format("\nEntity: %s, Request json: \n %s", entity, jsonPayload));
                if (!readOnly) {
                    try {
                        String restoreResponse = restoreEntity(jsonPayload, entity, versionTimestamp);
                        if (debugMode) {
                            JsonNode jsonResponse = objectMapper.readTree(restoreResponse);
                            response = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonResponse);
                            status.setRestoreRequestBody(jsonPayload);
                            status.setRestoreResponse(response);
                        }
                    } catch (Exception e) {
                        status.setExceptionMessage(e.getMessage());
                        status.setRestoreStatus(EntityRestoreStatus.RestoreStatus.FAILED);
                        continue;
                    }
                }
                status.setRestoreStatus(EntityRestoreStatus.RestoreStatus.SUCCESSFUL);
            
            } catch (Exception e) {
                String message = e instanceof ReltioAPICallFailureException ? ((ReltioAPICallFailureException) e).getErrorResponse() : (e instanceof GenericException ? ((GenericException) e).getExceptionMessage() : e.getMessage());
                status.setExceptionMessage(message);
                status.setRestoreStatus(EntityRestoreStatus.RestoreStatus.FAILED);
            }
            statusList.add(status);
        }
        return statusList;
    }
    
    private String restoreEntity(String payload, String entity, String timestamp) throws Exception {
        String postCall = tenantUrl + "/" + "entities";
        String response;
        try {
            response = apiService.post(postCall, payload);
            System.out.println(String.format("\nEntity: %s restored successfully to timestamp: %s", entity, timestamp));
        } catch (ReltioAPICallFailureException e) {
            System.out.println("Error Code: " + e.getErrorCode() + " >>>> Error Message: " + e.getErrorResponse());
            throw e;
        } catch (GenericException e) {
            throw e;
        }
        return response;
    }
    
    private String getHistoryTimestamp(String entityUri, String timestamp) throws Exception{
        String versionTimeStamp = null;
        String entityChangesUrl = tenantUrl+"/"+"entities"+"/"+entityUri+"/"+"_changesWithTotal";
        //filters
        List<String> filterOnTypes = Lists.newArrayList();
        //filter for type
        filterOnTypes.add("equals(type, ENTITY_CHANGED)");
        filterOnTypes.add("equals(type, RELATIONSHIP_CHANGED)");
        filterOnTypes.add("equals(type, RELATIONSHIP_CREATED)");
        filterOnTypes.add("equals(type, ENTITIES_MERGED)");
        filterOnTypes.add("equals(type, ENTITIES_MERGED_MANUALLY)");
        filterOnTypes.add("equals(type, ENTITIES_MERGED_ON_THE_FLY)");
        filterOnTypes.add("equals(type, ENTITIES_SPLITTED)");
        filterOnTypes.add("equals(type, ENTITY_CREATED)");
        filterOnTypes.add("equals(type, ENTITY_REMOVED)");
        filterOnTypes.add("equals(type, ENTITY_LOST_MERGE)");
        
        StringBuilder filterString = new StringBuilder("?filter=");
        for(int i=0; i<filterOnTypes.size(); i++) {
            filterString.append(filterOnTypes.get(i));
            if (i != (filterOnTypes.size() - 1)){
                filterString.append(" or ");
            }
        }
        //filter timestamp
        String filterTime = String.format("lte(timestamp, %s)", timestamp);
        filterString.append(" and " +filterTime);
        
        Map<String, String> requestParams = Maps.newHashMap();
        requestParams.put(SHOW_ALL, "true");
        requestParams.put(MAX_PARAM, "1000");
        requestParams.put(ORDER, "desc");
        requestParams.put(SORT, "timestamp");
        requestParams.put(FILTER, filterString.toString());
        
        for (Map.Entry<String, String> entry : requestParams.entrySet()) {
            filterString.append("&").append(entry.getKey()+"="+entry.getValue());
        }
        
        String changeList;
        
        try {
            changeList = apiService.get(entityChangesUrl+filterString);
            if (changeList !=null) {
                JsonNode jsonNode = objectMapper.readTree(changeList);
                versionTimeStamp = jsonNode.get("changes").get(0).get("timestamp").asText();
            }
        } catch (GenericException e) {
            throw e;
        } catch (ReltioAPICallFailureException e) {
            throw e;
        } catch (JsonMappingException e) {
            throw e;
        } catch (JsonProcessingException e) {
            throw e;
        }
        return versionTimeStamp;
    }
    
    private JsonNode getEntityAtTimestamp(String entityUri, String timestamp) throws Exception {
        String entityUrl = tenantUrl + "/" + "entities" + "/" + entityUri + "?time="+timestamp;
        String response;
        JsonNode entityJson = null;
        try {
            response = apiService.get(entityUrl);
            if (entityUrl != null) {
                ObjectMapper mapper = new ObjectMapper();
                entityJson = mapper.readTree(response);
            }
        } catch (GenericException e) {
            throw e;
        } catch (ReltioAPICallFailureException e) {
            throw e;
        } catch (JsonMappingException e) {
            throw e;
        } catch (JsonProcessingException e) {
            throw e;
        }
        return entityJson;
    }
    
    private boolean isReferenceAttribute(JsonNode attribute) {
        if ((attribute.getNodeType().equals(JsonNodeType.OBJECT)) && (attribute.has("refEntity") || attribute.has("refRelation"))) {
            return true;
        }
        return false;
    }
    
    private void processAttributes(ObjectNode jsonResult, JsonNode attributes, String sourceSystem) {
        if (attributes.getNodeType().equals(JsonNodeType.OBJECT)) {
            Iterator<Map.Entry<String, JsonNode>> iterator = attributes.fields();
            while (iterator.hasNext()) {
                Map.Entry<String, JsonNode> attribute = iterator.next();
                String name = attribute.getKey();
                JsonNode value = attribute.getValue();
                ArrayNode jsonArray = objectMapper.createArrayNode();
                if (attribute.getValue().getNodeType().equals(JsonNodeType.ARRAY)) {
                    Iterator<JsonNode> attributeValues = value.iterator();
                    while (attributeValues.hasNext()) {
                        JsonNode attributeValue = attributeValues.next();
                        JsonNode attributeValueNode  = attributeValue.get("value");
                        JsonNodeType attributeValueType = attributeValueNode.getNodeType();
                        ObjectNode attributeNode = objectMapper.createObjectNode();
                        if (attributeValueType.equals(JsonNodeType.OBJECT)) {
                            ObjectNode compexAttributeValue = objectMapper.createObjectNode();
                            processAttributes(compexAttributeValue, attributeValueNode, sourceSystem);
                            attributeNode.set("value", compexAttributeValue);
                        }
                        else if (attributeValueType.equals(JsonNodeType.STRING)) {
                            constructSimpleAttribute(attributeNode, attributeValueNode);
                        }
                        if(isReferenceAttribute(attributeValue)) {
                            constructReferenceAttribute(attributeNode, attributeValue, sourceSystem);
                        }
                        jsonArray.add(attributeNode);
                    }
                }
                jsonResult.set(name, jsonArray);
            }
        }
    }
    
    private void constructSimpleAttribute(ObjectNode value, JsonNode attributeValueNode) {
        value.put("value", attributeValueNode.asText());
    }
    
    private void constructReferenceAttribute(ObjectNode refAttribute, JsonNode attributeNode, String sourceSystem) {
        
        //process refEntity Crosswalks
        JsonNode refEntityType = attributeNode.get("refEntity").get("type");
        JsonNode refEntityCrosswalks = attributeNode.get("refEntity").get("crosswalks");
        JsonNode refEntityCrosswalk = sourceSystem != "Reltio" ? getCrosswalkFromSource(sourceSystem, refEntityCrosswalks) : null;
        String refEntityCrosswalkType = null;
        String refEntityCrosswalkValue = null;
        if (refEntityCrosswalk != null) {
            refEntityCrosswalkType = refEntityCrosswalk.get("type").asText();
            refEntityCrosswalkValue = refEntityCrosswalk.get("value").asText();
        }
        ArrayNode refEntityCrosswalkArray = objectMapper.createArrayNode();
        ObjectNode crossWalkResult = objectMapper.createObjectNode();
        crossWalkResult.put("type", refEntityCrosswalkType);
        crossWalkResult.put("value", refEntityCrosswalkValue);
        refEntityCrosswalkArray.add(crossWalkResult);
        
        //process refRelation Crosswalks
        JsonNode refRelationType = attributeNode.get("refRelation").get("type");
        JsonNode refRelCrosswalks = attributeNode.get("refRelation").get("crosswalks");
        JsonNode refRelationCrosswalk = sourceSystem != "Reltio" ? getCrosswalkFromSource(sourceSystem, refRelCrosswalks) : null;
        String refRelationCrosswalkType = null;
        String refRelationCrosswalkValue = null;
        if (refRelationCrosswalk != null) {
            refRelationCrosswalkType = refRelationCrosswalk.get("type").asText();
            refRelationCrosswalkValue = refRelationCrosswalk.get("value").asText();
        }
        ArrayNode refRelationCrosswalkArray = objectMapper.createArrayNode();
        ObjectNode relationCrossWalkResult = objectMapper.createObjectNode();
        relationCrossWalkResult.put("type", refRelationCrosswalkType);
        relationCrossWalkResult.put("value", refRelationCrosswalkValue);
        refRelationCrosswalkArray.add(relationCrossWalkResult);
        
        ObjectNode refEntityNode = objectMapper.createObjectNode();
        refEntityNode.set("type", refEntityType);
        refEntityNode.set("crosswalks", refEntityCrosswalkArray);
        
        ObjectNode refRelationNode = objectMapper.createObjectNode();
        refRelationNode.set("type", refRelationType);
        refRelationNode.set("crosswalks", refRelationCrosswalkArray);
        
        refAttribute.set("refEntity", refEntityNode);
        refAttribute.set("refRelation", refRelationNode);
        
    }
    
    private String getCrosswalkValueFromSource(String source, JsonNode crosswalksNode) {
        String sourceType = "configuration/sources/"+source;
        Iterator<JsonNode> crosswalks = crosswalksNode.iterator();
        while (crosswalks.hasNext()) {
            JsonNode crosswalkNode = crosswalks.next();
            if (sourceType != "Reltio" && crosswalkNode.get("type").asText().equals(sourceType)) {
                return crosswalkNode.get("value").asText();
            }
        }
        return null;
    }
    
    private JsonNode getCrosswalkFromSource(String source, JsonNode crosswalksNode) {
        String sourceType = "configuration/sources/"+source;
        Iterator<JsonNode> crosswalks = crosswalksNode.iterator();
        while (crosswalks.hasNext()) {
            JsonNode crosswalkNode = crosswalks.next();
            if (crosswalkNode.get("type").asText().equals(sourceType)) {
                return crosswalkNode;
            }
        }
        return null;
    }
    
    private ArrayNode constructCrosswalks(String source, String crosswalkValue) {
        ObjectNode objectNode = objectMapper.createObjectNode();
        objectNode.put("type", "configuration/sources/"+source);
        objectNode.put("value", crosswalkValue);
        ArrayNode arrayNode = objectMapper.createArrayNode();
        arrayNode.add(objectNode);
        return arrayNode;
    }
}
